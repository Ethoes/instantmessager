import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native'
//before running ~expo install expo-linear-gradient~
import LinearGradient from 'expo-linear-gradient';
import * as SQLite from 'expo-sqlite';
import { TouchableOpacity } from 'react-native-gesture-handler';

const db = SQLite.openDatabase("p5im.db");

class Convos extends React.Component {
  state = {
    people: null
  };

  componentDidMount() {
    this.update();
  }

  render() {
    const {people} = this.state;

    if(people === null || people.length === 0) {
      return null;
    }

    return(
      <View style={styles.sectionContainer}>
        {people.map(({id, userName}) => (
          <TouchableOpacity
            key={id}
            // onPress={}
            style={{
              backgroundColor: "#1c9963",
              borderColor: "#000",
              borderWidth: 1,
              padding: 8
            }}
            >
              <Text style={{color: "#fff"}}>{userName}</Text>
            </TouchableOpacity>
        ))}
      </View>
    );
  }

  update() {
    db.transaction(tx => {
      tx.executeSql(
        'select * from people',
        (_, {rows: {_array}}) => this.setState({people: _array})
      );
      tx.executeSql("select * from people", [], (_, { rows }) => 
        console.log(JSON.stringify(rows)));
    });
    console.log(this.state.people)
  }
}

export default class HomeScreen extends React.Component {
  //disables the navigation header
  static navigationOptions = {
    header: null,
  };

  componentDidMount() {

    //Creates a table in the database if it doesnt exist for the current convo.
    db.transaction(tx => {
      tx.executeSql(
          "create table if not exists people (id text, userName text)"
      );    
    });

    //remove this after one run, it adds a test object to the database.
    db.transaction(
      tx => {
        tx.executeSql("insert into people (id, userName) values (?, ?)", ["2", "Test"]);
    });
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <TouchableWithoutFeedback
          style={styles.body}
          onPress={() => navigate('Messager')}
        >
        <Image
        source={require('../assets/images/GameList.png')}
        
        />
        </TouchableWithoutFeedback>

        <Convos/>
      </View>
      // <ScrollView style={styles.listArea}>
      //   <Convos/>
      // </ScrollView>
    );
  }
}

var {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    height: height*0.074,
    width: width*0.67,
    left: width*0.165,
    top: height* -0.45,
    zIndex: 1,
    },
    Gradient: {
      padding: 15,
      alignItems:'center',
      height: height*1.1,
      width: width,
      top: height* -0.195,
      zIndex: -2,
    },
  flexRow: {
    flexDirection: "row"
  },
  input: {
    borderColor: "#4630eb",
    borderRadius: 4,
    borderWidth: 1,
    flex: 1,
    height: 48,
    margin: 16,
    padding: 8
  },
  listArea: {
    backgroundColor: "#f0f0f0",
    flex: 1,
    paddingTop: 16
  },
  sectionContainer: {
    marginBottom: 16,
    marginHorizontal: 16
  },
  sectionHeading: {
    fontSize: 18,
    marginBottom: 8
  }

});