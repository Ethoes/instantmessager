import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { GiftedChat } from "react-native-gifted-chat";

//run ~expo install expo-sqlite~ before running
import Constants from "expo-constants";
//import { SQLite } from "expo-sqlite";
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase("p5im.db");

export default class App extends React.Component {
  state = {
      //messages: null
    //Test messages.
    messages: [
      {
        _id: 1,
        text: "Your code is shit pussy!",
        createdAt: 12,
        user: {
            _id: 2,
        }
      },
      {
        _id: 2,
        text: "Suck ass",
        createdAt: 12,
        user: {
            _id: 1,
        },
      }
    ]
  };

  //When it opens the screen it runs this method and the starting routines.
  componentDidMount() {

      //Creates a table in the database if it doesnt exist for the current convo.
      db.transaction(tx => {
        tx.executeSql(
            "create table if not exists convo2 (id text, createdAt int)"
        );    
      });

      //Gets all messages from the database for the convo and then adds them to the screen. (needs to eb a limited amount of messages later)
      db.transaction(tx => {
        tx.executeSql(
          'select id from convo2 order by createdAt asc',
          [], (tx, results) => {
              this.setUp(results.rows._array);
          }
        );
    });  
  }

  //cycles through all messages and adds them to the message interface.
  setUp(results) {
    //console.log(results[0]);
    var i = 0;
    for(i = 0; i < results.length; i++) {
      //console.log("help");
      this.appendMessages(JSON.parse(results[i].id));
    }
  }

  //Method that adds a mesasge to the database, accepts (JSON, int)
  add(message, createdAt) {
      db.transaction(
          tx =>{
              tx.executeSql("insert into convo2 (id, createdAt) values (?, ?)", [message, createdAt]);
          },
          null,
      );
  }

  //Sends the message and appends it to the 
  sendMessage(message) {
      this.appendMessages(message);
      this.add(JSON.stringify(message), message[0].createdAt.getTime());
      this.send(message).catch(err => console.log(err));
  }

  //sends message to server
  async send(message) {
    const response = await fetch('http://10.0.1.177:5000/api/world', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(message)
  });
    const body = await response.json();
    if(response.status !== 200) throw Error(body.message);

    console.log(body);
  }

  //Appends the message so it appears on screen.
  appendMessages(message) {
      this.setState((previousState) => ({
          messages: GiftedChat.append(previousState.messages, message)
      }));
  }
  
  render() {
    return (
      <View style={styles.container}>
        <GiftedChat 
        messages={this.state.messages} 
        user={{
            _id: 1,
        }}
        onSend = {messages => this.sendMessage(messages)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});