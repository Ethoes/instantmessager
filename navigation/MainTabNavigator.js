//import screens needed to navigate
import HomeScreen from '../screens/HomeScreen';
import messager from '../screens/messager';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

//Create the navigator between the to screens
const MainNavigator = createStackNavigator(
  {
  Home: {screen: HomeScreen},
  Messager: {screen: messager},
  },
  //hide the tab on top so its a clean screen
//   {headerMode: 'none',}
);

const App = createAppContainer(MainNavigator);

export default App;