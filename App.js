import React from 'react';
import {StatusBar, StyleSheet, View} from 'react-native';
import AppNavigator from './navigation/AppNavigator';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <StatusBar hidden = {true}/> */}
        <AppNavigator/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backroundColor: '#fff',
  }
})